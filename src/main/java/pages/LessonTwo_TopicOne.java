package pages;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class LessonTwo_TopicOne extends NccdStarter
{
	public LessonTwo_TopicOne(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}

	/* Store all web Elements (Page objects - fields) */
	
	@FindBy(how=How.XPATH,using="//*[@id=\"maincontent\"]//following-sibling::h2")
	private WebElement t2pageHeader;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Start')]")
	private WebElement startBtn;
	
	@FindBy(how=How.XPATH,using="//span[contains(text(),'Start')]")
	private WebElement startOnAssessment;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Start')]")
	private WebElement startbtn;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Continue your last attempt')]")
	private WebElement continueYourLastAttmpt;
	
	@FindAll(@FindBy(how=How.XPATH, using = "//*[contains(@id,'quiznavbutton')]"))
	private List<WebElement> questions;
	
	@FindBy(how=How.XPATH,using="//*[@class='qtext']")
	private WebElement quesText;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//*[contains(@id,'answer')]"))
	private List<WebElement> options;
	
		
	@FindBy(how=How.XPATH,using="//input[@name=\"next\"]")
	private WebElement continueBtnAssessment;
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Continue')]")
	private WebElement continueBtn;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Try again')]")
	private WebElement tryAgainBtn;
	
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextLink;
	
	@FindBy(how=How.ID, using="h5p-iframe-610")
	private WebElement frameid;
	
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[starts-with(@id,'h5p-mcq')]"))
	private List<WebElement> quesno;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[@class='questionset started']//div[1]//div[2]//ul//li"))
	private List<WebElement> ansoptions;
	
	
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//a[starts-with(@id,'quiznavbutton')]"))
	private List<WebElement> progressBtn;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//input[@type='checkbox']"))
	private List<WebElement> checkBoxele;
	
	@FindBy(how=How.XPATH,using="//input[@value='Finish and submit']")
	private WebElement finishBtn;
	

	@FindBy(how=How.XPATH,using="//*[starts-with(@id,'q')]/div[2]/div/div[1]/h5")
	private WebElement qText;
	
	public LessonTwo_TopicOne executeLessontwoTopicsMethod() throws InterruptedException
	{
		getContent(t2pageHeader, "Lesson 2: Provisions of the Disability Standards for Education");
		System.out.println(t2pageHeader.getText());
		clickUsingJS(startBtn);
		getContent(t2pageHeader, "What's in Lesson 2?");
		System.out.println(t2pageHeader.getText());
		clickUsingJS(startOnAssessment);
		try 
		{
			if(continueYourLastAttmpt.isDisplayed())
			{
				System.out.println("Continue Your Last Attempt Button is Available");
				clickUsingJS(continueYourLastAttmpt);
				radioButtonSelfAssessment2( questions,quesText, options, continueBtnAssessment);
				clickUsingJS(continueBtn);
			}
			
		}
		catch (NoSuchElementException e) 
		{
			System.out.println("Try Again button is available");
			clickUsingJS(nextLink);
		}
		
		clickUsingJS(nextLink);
		clickUsingJS(nextLink);
		clickUsingJS(nextLink);
		switchtoframe(frameid);
		
		radioButtonselection3(quesno, ansoptions,2);
		driver.switchTo().defaultContent();
		clickUsingJS(nextLink);
		clickUsingJS(nextLink);
		clickUsingJS(nextLink);
		try 
		{
			if(continueYourLastAttmpt.isDisplayed())
			{
				System.out.println("Continue Your Last Attempt Button on Lesson 2 Assessment is Available");
				clickUsingJS(continueYourLastAttmpt);
				checkboxSelection_lessonTwo(progressBtn,checkBoxele,continueBtn,finishBtn,qText);
				//clickUsingJS(continueBtn);
			}
			
		}
		catch (NoSuchElementException e) 
		{
			if(tryAgainBtn.isDisplayed())
			{
				clickUsingJS(tryAgainBtn);
				//checkboxSelection_lessonTwo(progressBtn,checkBoxele,continueBtn,finishBtn,qText);
			}
			else
			{
			System.out.println("Start button is available");
			clickUsingJS(startbtn);
			clickUsingJS(nextLink);
			}
		}
		
		
		
		return this;
	}
	
		
}
