/**
 * 
 */
package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

/**
 * @author rhegde
 *
 */
public class TopicTwo_DDA_And_Standards extends NccdStarter
{
	public TopicTwo_DDA_And_Standards(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	/* Store all web Elements (Page objects - fields) */
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextLink;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"maincontent\"]//following-sibling::h2")
	private WebElement t2pageHeader;
	
	
	public TopicTwo_DDA_And_Standards executeTopicTwo() throws InterruptedException
	{
		getContent(t2pageHeader, "Topic 2: The DDA and the Standards");
		
		String stext = t2pageHeader.getText();
		System.out.println("Page header text is : " +stext);
		if(stext.equalsIgnoreCase("Topic 2: The DDA and the Standards"))
		{
			clickUsingJS(nextLink);
		}
		return this;
		
	}
}
