package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class CoursePage extends NccdStarter
{
	public CoursePage(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}

	@FindBy(how=How.XPATH, using="//*[@id=\"content\"]/div[2]/div[1]/div[5]/div[1]/ol/li[1]/a/span[2]")
	private WebElement part1_lesson1_startbtn;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"gdpr-agree\"]")
	private WebElement agree_btn;
	
	public CoursePage courseExecution()
	{
		clickButton(agree_btn);
		clickUsingJS(part1_lesson1_startbtn);
		return this;
		
	}
	
}
