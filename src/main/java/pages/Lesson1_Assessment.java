package pages;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class Lesson1_Assessment extends NccdStarter
{
	public Lesson1_Assessment(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}

	/* Store all web Elements (Page objects - fields) */
	
	@FindBy(how=How.XPATH,using="//*[@id=\"maincontent\"]//following-sibling::h2")
	private WebElement t2pageHeader;

	@FindAll(@FindBy(how=How.XPATH ,using= "//a[starts-with(@id,'quiznavbutton')]"))
	private List<WebElement> progressBtn;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"maincontent\"]//following-sibling::h2")
	private WebElement continueLastAttmpt;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Start')]")
	private WebElement startBtn;
	
	@FindBy(how=How.XPATH,using="//input[@value='Continue']")
	private WebElement continueBtn;
	
	@FindBy(how=How.XPATH,using="//input[@value='Finish and submit']")
	private WebElement finishBtn;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Continue your last attempt')]")
	private WebElement continueYourLastAttmpt;
	
	@FindBy(how=How.XPATH,using="//*[starts-with(@id,'q')]/div[2]/div/div[1]/p")
	private WebElement qText;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//input[@type='checkbox']"))
	private List<WebElement> checkBoxele;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Submit all and finish')]")
	private WebElement submitAllBtn;
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Try again')]")
	private WebElement tryAgainBtn;
	
	@FindBy(how=How.CSS,using="#page-mod-quiz-review > main > div > section > div.main-course--main-content > div > div > a")
	private WebElement finishReview;
	
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextLink;
	
	
	public Lesson1_Assessment executeLessonOneAssessmentmethod() throws InterruptedException
	{
		getContent(t2pageHeader, "Lesson 1: Assessment");
		
		try 
		{
			//boolean isAvailable = driver.findElementByXPath("//button[contains(text(),'Continue your last attempt')]").isDisplayed() ;
			if(driver.findElementByXPath("//button[contains(text(),'Continue your last attempt')]").isDisplayed())
			{
				System.out.println("Continue your last attempt button is available");
				clickUsingJS(continueYourLastAttmpt);
				
			}
		
		
		}
		catch (NoSuchElementException e) 
		{
			if(tryAgainBtn.isDisplayed())
			{
				clickUsingJS(tryAgainBtn);
			}
			else
			{
				System.out.println("Start button is available");
				//clickUsingJS(startbtn);
				//clickUsingJS(nextLink);
			}
			
		}
		
			String stext = qText.getText();
			System.out.println("Page Header Text is : " +stext);
			checkboxSelection_lessonOne(progressBtn,checkBoxele,continueBtn,finishBtn,qText);
			clickUsingJS(submitAllBtn);
			clickUsingJS(finishReview);
			clickUsingJS(nextLink);
	
		
		return this;
		
	}

}
