package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class EarlyChildhood_Lesson1 extends NccdStarter
{
	public EarlyChildhood_Lesson1(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	/* Store all web Elements (Page objects - fields) */
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextLink;
	
	@FindBy(how=How.XPATH,using="//h2[contains(text(),\"What's in Lesson 1?\")]")
	private WebElement pageHeader;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"page-mod-page-view\"]/main/div/section/div[1]/div/div[1]/div/div[2]/div")
	private WebElement SelfAssessmentBtn;
	
	@FindBy(how=How.XPATH,using="//*[text()='Continue your last attempt']")
	private WebElement ContinueBtn;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//*[contains(@id,'answer')]"))
	private List<WebElement> options;
	
	@FindAll(@FindBy(how=How.XPATH, using = "//*[contains(@id,'quiznavbutton')]"))
	private List<WebElement> questions;
	
	@FindBy(how=How.XPATH,using="//input[@name=\"next\"]")
	private WebElement continueBtnAssessment;
	
	@FindBy(how=How.XPATH,using="//*[text()='Continue']")
	private WebElement ContinueBtnOne;
	
	@FindBy(how=How.XPATH,using="//h4[@id='yui_3_17_2_1_1578886391597_100']")
	private WebElement headertext;
	

	
	@FindBy(how=How.XPATH, using="//h3[contains(text(),'Summary of your previous attempts')]")
	private WebElement attemptsummary;
	
		
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[starts-with(@id,'h5p-mcq')]"))
	private List<WebElement> quesno;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[@class='questionset started']//div[1]//div[2]//ul//li"))
	private List<WebElement> ansoptions;
	
		
	@FindBy(how=How.XPATH ,using= "//*[@class='h5p-question-next h5p-joubelui-button']")
	private WebElement nBtn;
	
	@FindBy(how=How.XPATH ,using= "//*[@class='h5p-question-content h5p-radio']")
	private WebElement answeroption;
	
	@FindBy(how=How.ID, using="h5p-iframe-606")
	private WebElement frameid;
	
	@FindBy(how=How.PARTIAL_LINK_TEXT, using="Topic 2: The DDA and")
	private WebElement nextTopic;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"page-mod-page-view\"]/main/div/section/div[1]/div/h2")
	private WebElement t2pageHeader;
	
	//Executes from Lesson 1 to Topic 1 complete //
		
	public EarlyChildhood_Lesson1 executeWholeCourse() throws InterruptedException
	{
		clickUsingJS(nextLink);
		getContent(pageHeader, "What's in Lesson 1?");
		clickButton(SelfAssessmentBtn);
		String stext = attemptsummary.getText();
		System.out.println("Page Header Text is : " +stext);
		if(stext.equalsIgnoreCase("Summary of your previous attempts"))
		{
			clickUsingJS(nextLink);
			clickUsingJS(nextLink);
			
		}
		else 
		{
			clickUsingJS(ContinueBtn);
			radioButtonSelfAssessment(questions,options,continueBtnAssessment);
		}
		
		
		/*String htext = headertext.getText();
		System.out.println("Page header text is : " +htext);
		if(htext.equalsIgnoreCase("How did you go? "))
		{
			clickUsingJS(ContinueBtnOne);
		}
		else
		{
			clickUsingJS(nextlink);
		}*/
		switchtoframe(frameid);
		//radioButtonselection(quesno,ansoptions,nBtn,2);
		radioButtonselection3(quesno, ansoptions,2);
		driver.switchTo().defaultContent();
		clickUsingJS(nextLink);
		getContent(t2pageHeader, "Topic 2: The DDA and the Standards");
		return this;
		
		
	}

	
}
