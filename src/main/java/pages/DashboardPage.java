package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class DashboardPage extends NccdStarter
{

	public DashboardPage(RemoteWebDriver driver) 
	{
		// TODO Auto-generated constructor stub
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"main\"]/div[1]/div/div/div/h1")
	private WebElement headerTitle;
	
	public DashboardPage verifyPageHeader(String data) throws InterruptedException
	{
		getContent(headerTitle, data);
		return this;
	
	}

}
