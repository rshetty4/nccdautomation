package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class TopicOne_DisabilityInAustralia extends NccdStarter
{
	public TopicOne_DisabilityInAustralia(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextlink;
	
	
	public TopicOne_DisabilityInAustralia navigateToLessonOneTopicOne() throws InterruptedException 
	{
		clickUsingJS(nextlink);
		return this;
		
	}

}
