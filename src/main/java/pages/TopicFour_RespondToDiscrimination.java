package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class TopicFour_RespondToDiscrimination extends NccdStarter
{
	public TopicFour_RespondToDiscrimination(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}

	
	/* Store all web Elements (Page objects - fields) */
	@FindBy(how=How.XPATH, using="//a[@id='next-activity-link']")
	private WebElement nextLink;
	
	@FindBy(how=How.XPATH,using="//*[@id=\"maincontent\"]//following-sibling::h2")
	private WebElement t2pageHeader;
	
	@FindBy(how=How.ID, using="h5p-iframe-608")
	private WebElement frameid;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[starts-with(@id,'h5p-mcq')]"))
	private List<WebElement> quesno;
	
	@FindAll(@FindBy(how=How.XPATH ,using= "//div[@class='questionset started']//div[1]//div[3]//ul//li"))
	private List<WebElement> ansoptions;
	
	@FindBy(how=How.XPATH ,using= "//a[@class='h5p-question-next h5p-joubelui-button']")
	private WebElement nBtn;
	
	@FindBy(how=How.XPATH ,using="//div[@class='questionset started']//div[1]//div[4]//a[1]")
			private WebElement nextBtn;
	
	
	public TopicFour_RespondToDiscrimination executeTopicFour() throws InterruptedException
	{
		getContent(t2pageHeader, "Topic 4: Respond to discrimination");
		
		String stext = t2pageHeader.getText();
		System.out.println("Page header text is : " +stext);
		if(stext.equalsIgnoreCase("Topic 4: Respond to discrimination"))
		{
			clickUsingJS(nextLink);
		}
				
		String nexttext = t2pageHeader.getText();
		System.out.println("Topic Header Text is : " +nexttext);
		switchtoframe(frameid);
		//radioButtonselection2(quesno);
		radioButtonselection3(quesno, ansoptions,3);
		driver.switchTo().defaultContent();
		clickUsingJS(nextLink);
		getContent(t2pageHeader, "Lesson 1: Assessment");
		return this;
	}

}
