package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import reusablefunctions.NccdStarter;

public class ProfessionalLearningPage extends NccdStarter
{
	public ProfessionalLearningPage(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//*[@id=\"block-nccd-main-menu\"]/ul/li[5]/span/button")
	private WebElement plMenu;

	
	@FindBy(how=How.LINK_TEXT, using = "Disability Standards for Education e-learning")
	private WebElement menuLinkText;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"content\"]/div[2]/div/section/div/div/div/h1")
	private WebElement plPageHeader;
	
	String plPageHeaderOne = "//*[@id=\"content\"]/div[2]/div/section/div/div/div/h1";
	
	@FindBy(how=How.LINK_TEXT, using = "Disability Standards for Education for early childhood (prior to school)")
	private WebElement resourceLinkText;
	
	String resourcePageHeader = "//*[@id=\"content\"]/div[2]/div[1]/h1";
	
	public ProfessionalLearningPage navigateToProfLearning() throws InterruptedException 
	{
		clickButton(plMenu);
		clickButton(menuLinkText);
		getContentTwo(plPageHeaderOne, "Professional learning");
		String actualresourceHeader = resourceLinkText.getText();
		clickUsingJS(resourceLinkText);
		System.out.println("Course Header/Name: "+ actualresourceHeader);
		getContentTwo(resourcePageHeader,actualresourceHeader);
		return this;
		
	}
	
	
}
