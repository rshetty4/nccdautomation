package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import io.qameta.allure.Step;
import reusablefunctions.NccdStarter;

public class NccdLoginPage extends NccdStarter
{
	public NccdLoginPage(RemoteWebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}
	
	
	/* Store all web Elements (Page objects - fields) */
	
	@FindBy(how=How.ID, using="username")
	private WebElement user_name;
	
	@FindBy(how=How.ID, using="password")
	private WebElement pwd;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"SSO-LoginForm\"]/div[3]/button")
	private WebElement btnSignIn;
	
	@FindBy(how=How.XPATH, using="//*[@id=\"main\"]/div[1]/div/div/div/h1")
	private WebElement headerTitle;
	
	String headerText = "//*[@id=\"main\"]/div[1]/div/div/div/h1";
	
	
	public NccdLoginPage doLogin() throws InterruptedException
	{
		populateLogin(user_name,pwd);
		clickButton(btnSignIn);
		getContentTwo(headerText, "Dashboard");
		Thread.sleep(4000);
		return this;
	
	}
	

	
	
	
}
