package tests;

import java.io.IOException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.DashboardPage;
import pages.NccdLoginPage;
import reusablefunctions.NccdStarter;
import reusablefunctions.ReusableFunctionsNCCD.Browser;

public class TC001_NccdLoginTestTest extends NccdStarter
{
	@Test
	public void nccdLoginTest() throws InterruptedException, IOException 
	{
		//browserSetup("https://sso.nccd.edu.au/simplesaml/module.php/core/loginuserpass.php?AuthState=_33daa2fa69753bbb952e1c61d541c24f265d240ff8%3Ahttps%3A%2F%2Fsso.nccd.edu.au%2Fsimplesaml%2Fsaml2%2Fidp%2FSSOService.php%3Fspentityid%3Ddrupal%26cookieTime%3D1575863009%26RelayState%3Dhttps%253A%252F%252Fwww.nccd.edu.au%252Fsaml_login");
		browserSetup();
		NccdLoginPage obj = new NccdLoginPage(driver);
		obj.doLogin();
		//driver.quit();
	}

   }
