package tests;

import java.io.IOException;

import org.testng.annotations.Test;

import pages.CoursePage;
import pages.EarlyChildhood_Lesson1;
import pages.NccdLoginPage;
import pages.ProfessionalLearningPage;
import pages.TopicFour_RespondToDiscrimination;
import pages.TopicThree_UnderstandAndRecogniseDisability;
import pages.TopicTwo_DDA_And_Standards;
import reusablefunctions.NccdStarter;

public class TC007_ExecuteTopicFour_lesson1 extends NccdStarter
{
	@Test
	public void profLearningNavigation() throws InterruptedException, IOException 
	{
		//Browser setup method
				browserSetup();
				
				//Enter Login credentials
				NccdLoginPage loginpage = new NccdLoginPage(driver);
				loginpage.doLogin();
				
				
				/* 
				 * Navigate to professional learning menu > select Disability Standards for learning menu item 
				 * List of Professional Learning are displayed
				 * Select 'Disability Standards for Education for early childhood (prior to school)' (first topic from the list)
				 * 
				 */
				ProfessionalLearningPage plearning =new ProfessionalLearningPage(driver);
				plearning.navigateToProfLearning();
			
				/* 
				 * Click on 'Start' button available on Lesson 1 
				 * 
				 */
				CoursePage cpexecution = new CoursePage(driver);
				cpexecution.courseExecution();
				
				
				/* 
				 * 
				 * User focus is on Lesson 1 , Click on 'Next' link
				 * Click on 'Start' button for Self assessment
				 * Lesson 1 Self Assessment is finished and submitted (iterate through the self assessment questions)
				 * If already submitted then navigate to next Topic (i.e. Topic 1)
				 * Then click on Next again (i.e. Check your awareness)
				 * On Check your awareness page, iterate through the awareness quiz and finish
				 * And then navigate to Next topic (i.e. Topic 2 : The DDA and the standards)
				 * 
				 */
				EarlyChildhood_Lesson1 eclesson = new EarlyChildhood_Lesson1(driver);
				eclesson.executeWholeCourse();
				
				/* 
				 * 
				 * User focus is on Lesson 1 - Topic 2 , Click on 'Next' link
				 * User is taken to the Next topic (i.e. Topic 3 : Understand and recognise disability)
				 * 
				 */
				TopicTwo_DDA_And_Standards topictwo = new TopicTwo_DDA_And_Standards(driver);
				topictwo.executeTopicTwo();
				
				/* 
				 * 
				 * User focus is on Lesson 1 - Topic 3 , Click on 'Next' link
				 * User is taken to the Next topic (i.e. Recognising disability)
				 * 
				 */
				TopicThree_UnderstandAndRecogniseDisability topicthree = new TopicThree_UnderstandAndRecogniseDisability(driver);
				topicthree.executeTopicThree();
				
				/* 
				 * 
				 * User focus is on Topic 4 , Click on 'Next' link
				 * User is taken to the Next topic (i.e. Recognising discrimination)
				 * 
				 */
				TopicFour_RespondToDiscrimination topicfour = new TopicFour_RespondToDiscrimination(driver);
				topicfour.executeTopicFour();
				
	}
}
