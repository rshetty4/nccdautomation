package tests;

import java.io.IOException;

import org.testng.annotations.Test;

import pages.CoursePage;
import pages.EarlyChildhood_Lesson1;
import pages.NccdLoginPage;
import pages.ProfessionalLearningPage;
import pages.TopicOne_DisabilityInAustralia;
import reusablefunctions.NccdStarter;

public class TC004_Lesson1_EarlyChildhood_priortoSchool_execution extends NccdStarter
{
	@Test
	public void LessonOne_EA_priortoschool() throws InterruptedException, IOException 
	{
		
		browserSetup();
		NccdLoginPage loginpage = new NccdLoginPage(driver);
		loginpage.doLogin();
		ProfessionalLearningPage plearning =new ProfessionalLearningPage(driver);
		plearning.navigateToProfLearning();
		
		CoursePage cpexecution = new CoursePage(driver);
		cpexecution.courseExecution();
		
		EarlyChildhood_Lesson1 eclesson = new EarlyChildhood_Lesson1(driver);
		eclesson.executeWholeCourse();
		
		TopicOne_DisabilityInAustralia topicOne = new TopicOne_DisabilityInAustralia(driver);
		topicOne.navigateToLessonOneTopicOne();
		//driver.quit();
	}

}
