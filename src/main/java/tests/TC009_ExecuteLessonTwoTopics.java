package tests;

import java.io.IOException;

import org.testng.annotations.Test;

import pages.CoursePage;
import pages.EarlyChildhood_Lesson1;
import pages.Lesson1_Assessment;
import pages.LessonTwo_TopicOne;
import pages.NccdLoginPage;
import pages.ProfessionalLearningPage;
import pages.TopicFour_RespondToDiscrimination;
import pages.TopicThree_UnderstandAndRecogniseDisability;
import pages.TopicTwo_DDA_And_Standards;
import reusablefunctions.NccdStarter;

public class TC009_ExecuteLessonTwoTopics extends NccdStarter
{
	@Test
	public void executeLessonTwoTopics() throws InterruptedException, IOException 
	{
		
				
				LessonTwo_TopicOne lessonTwoTopic = new LessonTwo_TopicOne(driver);
				lessonTwoTopic.executeLessontwoTopicsMethod();
	}
}
