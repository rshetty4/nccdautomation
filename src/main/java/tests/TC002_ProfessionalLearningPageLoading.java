package tests;

import java.io.IOException;

import org.testng.annotations.Test;

import pages.NccdLoginPage;
import pages.ProfessionalLearningPage;
import pages.NccdLoginPage;
import reusablefunctions.NccdStarter;

public class TC002_ProfessionalLearningPageLoading extends NccdStarter
{
	@Test
	public void profLearningNavigation() throws InterruptedException, IOException 
	{
		//browserSetup("https://sso.nccd.edu.au/simplesaml/module.php/core/loginuserpass.php?AuthState=_33daa2fa69753bbb952e1c61d541c24f265d240ff8%3Ahttps%3A%2F%2Fsso.nccd.edu.au%2Fsimplesaml%2Fsaml2%2Fidp%2FSSOService.php%3Fspentityid%3Ddrupal%26cookieTime%3D1575863009%26RelayState%3Dhttps%253A%252F%252Fwww.nccd.edu.au%252Fsaml_login");
		browserSetup();
		NccdLoginPage loginpage = new NccdLoginPage(driver);
		loginpage.doLogin();
		ProfessionalLearningPage plearning =new ProfessionalLearningPage(driver);
		plearning.navigateToProfLearning();
		
		//driver.quit();
	}
}
