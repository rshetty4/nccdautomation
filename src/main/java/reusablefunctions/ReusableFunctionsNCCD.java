/**
 * 
 */
package reusablefunctions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * @author rhegde
 *
 */
public class ReusableFunctionsNCCD 
{
	protected static RemoteWebDriver driver;
	public String URL, browser, uname, pwd;
		
	/* Constructor to load all global properties */
	
	public ReusableFunctionsNCCD()
	{
		Properties prop = new Properties();
		try 
		{
			prop.load(new FileInputStream(new File("./src/main/resources/global.properties")));
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		URL= prop.getProperty("URL");
		uname = prop.getProperty("Username");
		pwd = prop.getProperty("Password");
		
	}
	
	/* Browser setup - enum */
	
    public  void browserSetup() throws InterruptedException 
    {
		
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(URL);
		Thread.sleep(5000);
	}

    /* Populate Login details method */
	public void populateLogin(WebElement unameElement, WebElement pwdElement)
	{
		enterText(unameElement,uname);
		enterText(pwdElement,pwd);
		System.out.println("USERNAME is : "+ uname);
		System.out.println("PASSWORD is : "+ pwd);
	}
	
	public enum Browser{CHROME, FIREFOX, EDGE}
	
	/* Open browser Method */
	public WebDriver OpenBrowser(Browser browser)
	{
		if(browser == browser.CHROME)
		{
			WebDriverManager.chromedriver().setup();
			driver= new ChromeDriver();
		}
		else
			//(browser == browser.FIREFOX)
		{
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		//else
		//{
		//	WebDriverManager.edgedriver().setup();
		//	driver = new EdgeDriver();
		//}
		driver.get(URL);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	
	// Enter Text to a text field method
	public void enterText(WebElement element, String data)
	{
		try 
		{
			element.clear();
			element.click();
			element.sendKeys(data);
		} 
		catch(InvalidElementStateException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(WebDriverException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Click button method  using explicitWait 
	public void clickButton(WebElement element)
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(element)).click();
	}
	
	
	//Use JavascriptExecutor to send the click directly on the element (Element is present but having permanent Overlay) .Ask Dau!
	
	public void clickUsingJS(WebElement element)
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOf(element));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
	}
	
	// Radio button selection method for self assessment -
	public void radioButtonSelfAssessment(List<WebElement> questions, List<WebElement> options, WebElement continueBtnAssessment)
	{
	//	WebDriverWait wait = new WebDriverWait(driver, 10);
		int noofques=questions.size();
		//System.out.println("Progress List size is: " +noofques);
		//int optioncount = options.size();
		//System.out.println("Option on Question 1 : " +optioncount);
		for(int i=0; i<noofques;i++)
		{
			questions.get(i).click();
			String text= driver.findElementByXPath("/html/body/main/div/section/div[2]/div/form/div/div[1]/div[1]/h3").getText();
			List<WebElement> radioselection = driver.findElements(By.xpath("//*[contains(@id,'answer')]"));
			int btnCount = radioselection.size();
			inner:for (int j=0;j<btnCount;)
			{
				String radioname1=radioselection.get(j).findElement(By.xpath("//*[contains(@class,'answernumber')]")).getText();
				if(radioselection.get(j).isSelected())
				{
					clickUsingJS(continueBtnAssessment);
					break inner;
				}
				else 
				{
					radioselection.get(j).click();
					clickUsingJS(continueBtnAssessment);
					break inner;
				}
			}
			
		}
		WebElement lastbtn1 = driver.findElementByXPath("//*[text()='Submit all and finish']");
		clickUsingJS(lastbtn1);
	}
	
	
	
			
/*	DAUPADI's Code
 * 
 * public void radioButtonselection2(List<WebElement> quesno)
			throws InterruptedException {
		WebElement question = driver
				.findElement(By
						.xpath("/html/body/div/div/div/div/div[2]/div[1]/div[3]/ul/li[1]/div[1]"));
		clickUsingJS(question);
		Thread.sleep(4000);
		WebElement next = driver.findElement(By
				.xpath("//a[@class='h5p-question-next h5p-joubelui-button']"));
		clickUsingJS(next);
		int noofques = quesno.size();
		for (int i = 2; i < noofques; i++) {
			System.out.println("Question no" + i);
			question = driver.findElement(By
					.xpath("/html/body/div/div/div/div/div[2]/div[" + i
							+ "]/div[3]/ul/li[1]/div[1]"));
			clickUsingJS(question);
			System.out.println("clicked the answer");
			boolean isAvailable = driver
					.findElement(
							By.cssSelector("body > div > div > div > div > div.questionset.started > div:nth-child("
									+ i
									+ ") > div.h5p-question-buttons.h5p-question-visible > a.h5p-question-next.h5p-joubelui-button"))
					.isDisplayed();
			System.out.println(isAvailable);
			if (isAvailable) {
				System.out.println("Next button is available");
				WebElement ele = driver
						.findElement(By
								.cssSelector("body > div > div > div > div > div.questionset.started > div:nth-child("
										+ i
										+ ") > div.h5p-question-buttons.h5p-question-visible > a.h5p-question-next.h5p-joubelui-button"));
				clickUsingJS(ele);
			} else if (!isAvailable) {
				System.out.println("Next button is not available");
				WebElement finish = driver
						.findElement(By
								.xpath("//a[@class='h5p-question-finish h5p-joubelui-button']"));
				clickUsingJS(finish);
			}
			Thread.sleep(5000);
		}
		WebElement lastquestion = driver
				.findElement(By
						.xpath("/html/body/div/div/div/div/div[2]/div[4]/div[3]/ul/li[1]/div[1]"));
		clickUsingJS(lastquestion);
		Thread.sleep(4000);
		WebElement finish = driver
				.findElement(By
						.cssSelector("body > div > div > div > div > div.questionset.started > div:nth-child(4) > div.h5p-question-buttons.h5p-question-visible > button.h5p-question-finish.h5p-joubelui-button"));
		clickUsingJS(finish);
	}	
	*/
	
	public void switchtoframe(WebElement frameid2) 
	{
		// TODO Auto-generated method stub
		driver.switchTo().frame(frameid2);
		
	}
	

	public void jsexecutormethod(WebElement element)
	{
		JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
	}
	// Hover and Click Method
	public void HoverAndClick(WebDriver driver,WebElement elementToHover,WebElement elementToClick) 
	{
		//WebDriverWait wait = new WebDriverWait(driver, 60);
		Actions action = new Actions(driver);
		action.moveToElement(elementToHover).click(elementToClick).build().perform();
	}
	
	
	// Get content/text Method
	public void getContent(WebElement element, String text) throws InterruptedException 
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String content = element.getText();
		Assert.assertTrue(content.contains(text));
	}
	
	public void getContentTwo(String Labelxpath, String text) throws InterruptedException 
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String content = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Labelxpath))).getText();
		Assert.assertTrue(content.contains(text));
		//System.out.println("Inside getcontent - Content is  : "+ content);
	}
	
	// radio button - random selection of options (first one)
	
	public void radioButtonselection3(List<WebElement> quesno, List<WebElement> ansoptions, int val)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		int noofques = quesno.size();
		System.out.println(
				"No of Questions are: " + noofques + " and no.of options on question 1 is : " + ansoptions.size());
		for (int i = 1; i <= noofques; i++) {
			List<WebElement> radioselection = driver.findElements(
					By.xpath("//div[@class='questionset started']//div[" + (i) + "]//div[" + val + "]//ul//li"));
			Thread.sleep(4000);
			int ansCount = radioselection.size();
			System.out.println("I value is :" + i + " No of radio/answers: " + ansCount);
			WebElement selectOption = driver.findElement(
					By.xpath("//div[@class='questionset started']//div[" + (i) + "]//div[" + val + "]//ul//li[1]"));
			clickUsingJS(selectOption);
			System.out.println("clicked the answer");
			try {
				boolean isAvailable = driver.findElement(
						By.cssSelector("div.questionset.started > div:nth-child(" + i
								+ ") > div.h5p-question-buttons.h5p-question-visible > a.h5p-question-next.h5p-joubelui-button"))
						.isDisplayed();
				System.out.println(isAvailable);
				if (isAvailable) {
					System.out.println("Next button is available");
					WebElement ele = driver.findElement(
							By.cssSelector("div.questionset.started > div:nth-child(" + i
									+ ") > div.h5p-question-buttons.h5p-question-visible > a.h5p-question-next.h5p-joubelui-button"));
					clickUsingJS(ele);

				}
			}

			catch (NoSuchElementException e) {
				System.out.println("Next button is not available");
				WebElement finish = driver
						.findElement(By.xpath(" //button[@class='h5p-question-finish h5p-joubelui-button']"));
				clickUsingJS(finish);
			}

		}
	}
	
	
	/*
	 * Lesson 1 Assessment checkbox selection
	 */
	
	public void checkboxSelection_lessonOne(List<WebElement> progressBtn,List<WebElement> checkBoxelements,WebElement continueBtn, WebElement finishBtn, WebElement qText )
	{
		int noofques = progressBtn.size();
		System.out.println("No of Questions is: " + noofques + " and no.of options on question 1 is : " + checkBoxelements.size());
		for(int i=0;i<noofques;i++)
		{
			progressBtn.get(i).click();
			
			switch (i)
			{
				case 0:
					System.out.println("CASE 0");
					System.out.println("1." +qText.getText());
					checkBoxelements.get(0).click();
					checkBoxelements.get(1).click();
					checkBoxelements.get(2).click();
					clickUsingJS(continueBtn);
				case 1:
					System.out.println("Inside Else If");
					System.out.println("1." +qText.getText());
					checkBoxelements.get(0).click();
					checkBoxelements.get(1).click();
					checkBoxelements.get(3).click();
			}
		}
		clickUsingJS(finishBtn);
	}
	
	
	public void checkboxSelection_lessonTwo(List<WebElement> progressBtn,List<WebElement> checkBoxelements,WebElement continueBtn, WebElement finishBtn, WebElement qText )
	{
		int noofques = progressBtn.size();
		System.out.println("No of Questions is: " + noofques + " and no.of options on question 1 is : " + checkBoxelements.size());
		for(int i=0;i<noofques;i++)
		{
			progressBtn.get(i).click();
			
			switch (i)
			{
				case 0:
					System.out.println("CASE 0");
					System.out.println("1." +qText.getText());
					checkBoxelements.get(1).click();
					checkBoxelements.get(3).click();
					clickUsingJS(continueBtn);
					//break;
				case 1:
					System.out.println("CASE 1");
					System.out.println("1." +qText.getText());
					checkBoxelements.get(0).click();
					checkBoxelements.get(1).click();
					checkBoxelements.get(2).click();
					//break;
				
			}
			
		}
		clickUsingJS(finishBtn);
	}
	
	
	// Radio button selection method for self assessment -
		public void radioButtonSelfAssessment2(List<WebElement> questions,WebElement quesText, List<WebElement> options, WebElement continueBtnAssessment)
		{
			int noofques=questions.size();
			System.out.println("No of Questions : "+noofques);
			for(int i=0; i<noofques;i++)
			{
				questions.get(i).click();
				System.out.println("Question Text: "+quesText.getText());
				System.out.println("No of options on the question:"+options.size());
				if(options.get(3).isSelected())
				{
					System.out.println("Already selected");
					clickUsingJS(continueBtnAssessment);
				}
				else
				{
					System.out.println("Not selected, selecting the last option");
					options.get(3).click();
					clickUsingJS(continueBtnAssessment);
				}
				
		
			}
			WebElement lastbtn1 = driver.findElementByXPath("//*[text()='Submit all and finish']");
			clickUsingJS(lastbtn1);
		}
}
		
	

